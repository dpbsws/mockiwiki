<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Mail;
use App\Models\Homesection;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:api');        
    }
    
    //Student login
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return \response()->json(['error' => $validator->errors(), 'data' => false], 400);
        }

        $student = Student::where('email', $request->post('email'))->first();

        $salt = "DYhG93b0qyJfIgbf4567fghuVoUubWwvniR2G0FgaC9mi";
        // echo Hash::make($str, ['algo'=>'sha256']);
        // bin2hex(mhash(MHASH_SHA256, $string))
        // echo hash('sha256', $str);  
        // echo bin2hex(mhash(MHASH_SHA256, $str));   

        if ($student) {
            if ($student->password == hash('sha256', ($salt.$request->post('password')))) {
                if($student->photo!=''){
                    $student->photo = "https://mockiwiki.com/app/webroot/img/student/".$student->photo;
                }
                return response()->json([
                    'error' => false,
                    'data'  => [
                        'student' => $student
                    ]
                ]);
            }else{
                Hash::check();
                return response()->json([
                    'error' => true,
                    'message' => 'Incorrect password',
                    'data'  => false
                ]);
            }
            
        }else{
            return response()->json([
                'error' => true,
                'message' => 'Incorrect login details',
                'data'  => false
            ]);
        }

        // if ($request->post('password') == Hash::check()) {
        //     # code...
        // }
    }
    
    //Student registration
    public function register(Request $request)
    {
        $salt = "DYhG93b0qyJfIgbf4567fghuVoUubWwvniR2G0FgaC9mi";
        $random_cade = generate_rand();

        $validator = Validator::make($request->all(),[
            'email' => 'required|email',
            'phone' => 'required|digits:10',
            'groups' => 'required|array',
            'alternative_phone' => 'digits:10',
            'name' => 'required|string',
            'enroll' => 'string|min:50',
            'password' => 'required|string|min:8',
            'reserve_category' => 'required|string|max:50',
            'address' => 'required|string|max:200',
            'language' => 'required|string|max:20',
            'dob' => 'required|date',
            'qualification' => 'string|min:50',
            'city' => 'string|min:20',
            'gender' => 'required|string|min:1',
            'avatar' => 'sometimes|image|mimes:jpg,jpeg,png|max:1024'
        ]);

        $student = Student::create([
            'email' => $request->post('email'),
            'phone' => $request->post('phone'),
            'guardian_phone' => $request->post('alternative_phone'),
            'name' => $request->post('name'),
            'enroll' => $request->post('enroll'),
            'password' => hash('sha256', ($salt.$request->post('password'))),
            'category' => $request->post('reserve_category'),
            'address' => $request->post('address'),
            'language' => $request->post('language'),
            'b_date' => $request->post('dob'),
            'qualification' => $request->post('qualification'),
            'city' => $request->post('city'),
            'gender' => $request->post('gender'),
            'reg_code' => $random_cade,
            'renewal_date' => Carbon::now('Asia/Kolkata'),
        ]);

        if ($student) {
            $selected_groups = $request->post('groups');
            $arr = [];
            foreach ($selected_groups as $key => $value) {
                $arr[] = [
                    'student_id' => $student->id,
                    'group_id' => $value,
                ];
            }

            DB::table('student_groups')->insert($arr);
        }



        if ($validator->fails()) {
            return \response()->json(['error' => $validator->errors(), 'data' => false], 400);
        }


    }

    public function exam_groups()
    {
        $exam_groups = DB::table('groups')->select('id', 'group_name')->get();
        if ($exam_groups) {
            return response()->json([
                'error' => false,
                'message' => 'success',
                'data'  => [
                    'exam_groups' => $exam_groups
                ]
            ]);
        }else{
            return response()->json([
                'error' => true,
                'message' => 'data not found',
                'data'  => false
            ]);
        }
    }

    public function reserve_categories()
    {
        $r_cat_arr = [
            'general' => 'General',
            'obc' => 'Obc',
            'sc' => 'SC',
            'st' => 'ST',
            'pd' => 'PD',
            'sports' => 'Sports',
            'defence' => 'Defence',
            'others'=>'Others'
        ];

        return response()->json([
            'error' => true,
            'message' => 'success',
            'data'  => [
                'reserve_categoies' => $r_cat_arr
            ]
        ]);
    }

    public function prefer_lang()
    {
        $p_lang = ['English' => 'English', 'Hindi' => 'Hindi'];

        return response()->json([
            'error' => true,
            'message' => 'success',
            'data'  => [
                'prefer_lang' => $p_lang
            ]
        ]);
    }

    public function getBannerList()
    {
        $banners = Homesection::where('id','=',1)->get()->first();
        $image = $banners->sections_img;
        $image = "https://mockiwiki.com/app/webroot/img/tab/".$image;
        $image2 = "https://mockiwiki.com/app/webroot/img/tab/banner3.png";
        $image3 = "https://mockiwiki.com/app/webroot/img/tab/banner4.png";

        $banner_images = array("image"=>$image, "image"=>$image2, "image"=>$image3);
        
        $imgs = [
            0 => array("image" => $image),
            1 => array("image" => $image2),
            2 => array("image" => $image3),
        ];
        
        if($banners){
        return response()->json([
            'error' => false,
            'message' => 'success',
            'data'  => $imgs
        ]);
        }
        else{
        return response()->json([
            'error' => true,
            'message' => 'fail',
            'data'  => []
        ]);
            
        }
    }
    
    public function getProfileDetail(Request $request){
        $user_id = $request->input('user_id');
        $user = Student::where('id','=',$user_id)
                              ->select('email','name','address','phone','gender','b_date','city','category','qualification','language','guardian_phone','enroll','photo','reg_code','reg_status','aadhar_no','academic_year','admission_class','old_admission_no','parent_email','mother_name')
                              ->get();
        if(count($user)==0){
        return response()->json([
            'error' => true,
            'message' => 'No student found',
            'data'  => []
        ]);
        }
        else{
        $new_data = [];
        foreach($user as $u){
            if($u->photo!=''){
                $u->photo = "https://mockiwiki.com/app/webroot/img/student/".$u->photo;
            }
        }    
        
            
        return response()->json([
            'error' => false,
            'message' => 'success',
            'data'  => [
                'user_info' => $user
            ]
        ]);
            
        }
    }

    public function inboxAllMessageList(Request $request){
        $error=true;
        $message = 'fail';
        $data=[];

        $to_user_id = $request->input('user_id');
        if($to_user_id=='' || empty($to_user_id)){
            $error = true;    
            $message = 'No user_id found';
            $data=[];
        }
        else{
        $user = Student::where('id','=',$to_user_id)->get()->first();
        $email = '';
        if(!$user){
            $error = true;    
            $message = 'No student found';
            $data=[];
        }
        else{
            $email = $user->email;
            $mails = Mail::where('to_email','=',$email)
                         ->select('to_email','from_email','email','subject','message','date','status','type','mail_type')
                           ->get();
            if(count($mails)==0)
            {
            $error = true;    
            $message = 'No mails found';
            $data=[];
            }
            else{
            $error = false;    
            $message = 'success';
            $data=$mails;
            }
        }
        }
        
        return response()->json([
            'error' => $error,
            'message' => $message,
            'data'  => $data
        ]);
    }

    public function sentMessageList(Request $request){
        $error=true;
        $message = 'fail';
        $data=[];

        $from_user_id = $request->input('user_id');
        if($from_user_id=='' || empty($from_user_id)){
            $error = true;    
            $message = 'No user_id found';
            $data=[];
        }
        else{
        $user = Student::where('id','=',$from_user_id)->get()->first();
        $email = '';
        if(!$user){
            $error = true;    
            $message = 'No student found';
            $data=[];
        }
        else{
            $email = $user->email;
            $mails = Mail::where('from_email','=',$email)
                         ->select('to_email','from_email','email','subject','message','date','status','type','mail_type')
                          ->get();
            if(count($mails)==0)
            {
            $error = true;    
            $message = 'No mails found';
            $data=[];
            }
            else{
            $error = false;    
            $message = 'success';
            $data=$mails;
            }
        }
        }
        
        return response()->json([
            'error' => $error,
            'message' => $message,
            'data'  => $data
        ]);
    }

    public function OneToOneMessageList(Request $request){
        //Setting defaults
        $error=true;
        $message = 'fail';
        $data=[];
        
        //Getting user inputs
        $from_user_id = $request->input('from_user_id');
        $to_user_id = $request->input('to_user_id');
        
        //If from user id and to user id not found
        if(($from_user_id=='' || empty($from_user_id)) && ($to_user_id=='' || empty($to_user_id))){
            $error = true;    
            $message = 'No user_id found';
            $data=[];
        }
        else{
            //if both user ids found
            
            if($from_user_id!='0'){
                //Not adminstrator
                
                $from_user = Student::where('id','=',$from_user_id)->get()->first();
                if(!$from_user){
                    return response()->json([
                        'error' => true,
                        'message' => 'From user not found',
                        'data'  => []
                    ]);
                }
            }
            else{
                $from_user = "Administrator";    
            }
            
            if($to_user_id!='0'){
                $to_user = Student::where('id','=',$to_user_id)->get()->first();
                if(!$to_user){
                    return response()->json([
                        'error' => true,
                        'message' => 'To user not found',
                        'data'  => []
                    ]);
                }
            }
            else{
                $to_user = "Administrator";                    
            }
            
        $email = '';
        if(!$from_user && !$to_user){
            $error = true;    
            $message = 'No student found';
            $data=[];
        }
        else{
            
            if($from_user !='Administrator'){
                $from_email = $from_user->email;
            }
            else{
                $from_email = 'Administrator';
            }

            if($to_user !='Administrator'){
                $to_email = $to_user->email;
            }
            else{
                $to_email = 'Administrator';
            }
            
            $mails = Mail::where('from_email','=',$from_email)
                         ->where('to_email','=',$to_email)
                         ->select('to_email','from_email','email','subject','message','date','status','type','mail_type')
                         ->get();
            
            if(count($mails)==0)
            {
            $error = true;    
            $message = 'No mails found';
            $data=[];
            }
            else{
            $error = false;    
            $message = 'success';
            $data=$mails;
            }
        }
        }
        
        return response()->json([
            'error' => $error,
            'message' => $message,
            'data'  => $data
        ]);
    }
    
    public function getRegistrationFormData(Request $request){
        $reservation = ['general' => 'General', 'obc' => 'Obc','sc' => 'SC','st' => 'ST','pd' => 'PD','sports' => 'Sports', 'defence' => 'Defence','others'=>'Others'];
        $pref_languages = ['English' => 'English', 'Hindi' => 'Hindi'];
        return response()->json([
            'error' => false,
            'message' => 'message',
            'data'  => [
                'groups'=> DB::table('groups')->select('id', 'group_name')->get(),
                'reservation-categories' => $reservation,
                'preferred-languages' => $pref_languages
            ]
        ]);
    }
    
    public function studentProfileUpdate(Request $request){
        //Setting defaults
        $error=true;
        $message = 'fail';
        $data=[];
        
        $user_id = $request->input('user_id');
        if($user_id==''){
            $error=true;
            $message = 'Please provide user id';
            $data=[];
        }
        else{
            //Updates enroll no, phone no,alternate no,address
            $enroll = $request->input('enrollment_no');
            $phone = $request->input('phone');
            $guardian_phone = $request->input('guardian_phone');
            $address = $request->input('address');
            
            // if($enroll!='' && $phone!='' && $guardian_phone!='' && $address!='')
            // {
                
                $user = Student::where('id','=',$user_id)->get()->first();
                if($user){
                
                $student = Student::find($user_id);
                $student->enroll = $request->enrollment_no;
                $student->phone = $request->phone;
                $student->guardian_phone = $request->guardian_phone;
                $student->address = $request->address;
                $student->b_date = $request->birth_date;
                $student->aadhar_no = $request->aadhar_no;
                $student->academic_year = $request->academic_year;
                $student->admission_class = $request->admission_class;
                $student->old_admission_no = $request->old_admission_no;                
                $student->parent_email = $request->parent_email;
                $student->mother_name = $request->mother_name;                                
                $student->save();
            
                $error=false;
                $message = 'Data updated';
                $data=[];
                }
                else{
                $error=true;
                $message = 'Student not found';
                $data=[];
                }
                
            // }
            // else{
            //     $error=true;
            //     $message = 'Fields cannot be empty';
            //     $data=[];
            // }
        }
        return response()->json([
            'error' => $error,
            'message' => $message,
            'data'  => $data
        ]);
    }

    public function composeMail(Request $request){
        //Setting defaults
        $error=true;
        $message = 'fail';
        $data=[];
        
        $user_id = $request->input('user_id');
        if($user_id==''){
            $error=true;
            $message = 'Please provide user id';
            $data=[];
        }
        else{
            //Updates enroll no, phone no,alternate no,address
            $user_id = $request->input('user_id');
            $subject = $request->input('subject');
            $msg = $request->input('message');

            if($subject!='' && $msg!='')
            {
                
                $user = Student::where('id','=',$user_id)->get()->first();
                if($user){
                    $from_email = $user->email;
                    
                    $mail = new Mail;
                    $mail->to_email = 'Administrator';
                    $mail->from_email = $from_email;
                    $mail->email = $from_email;
                    $mail->subject = $subject;
                    $mail->message = $msg;
                    $mail->date = date('Y-m-d h:i:s');
                // $student = Student::find($user_id);
                // $student->enroll = $request->enrollment_no;
                // $student->phone = $request->phone;
                // $student->guardian_phone = $request->guardian_phone;
                // $student->address = $request->address;
                    $mail->save();
            
                $error=false;
                $message = 'Email sent successfully';
                $data=[];

                }
                else{
                $error=true;
                $message = 'Student not found';
                $data=[];
                }
                
            }
            else{
                $error=true;
                $message = 'Fields cannot be empty';
                $data=[];
            }
        }
        return response()->json([
            'error' => $error,
            'message' => $message,
            'data'  => $data
        ]);
    }
    
    public function getPackageCategoryList(){
        $packcats = DB::table('packcats')->select('id','category_name','photo')->get();
        if(count($packcats)>0){

                $pck = [];
                
                foreach($packcats as $p){
                    // $p->photo = "https://mockiwiki.com/app/webroot/img/package/".$p->photo;
                    $ipc = [
                        'id'=>$p->id,
                        'category_name'=>$p->category_name,
                        'photo'=>$p->photo,
                    ];
                    array_push($pck,$ipc);
                }

        return response()->json([
            'error' => false,
            'message' => 'success',
            'data'  => $packcats,
            'count' => count($pck)
        ]);        
        }
        else{
            return response()->json([
                'error' => true,
                'message' => 'No data found',
                'data'  => [],
            ]);        
        }
    }

    public function getPackagesByCategory(Request $request){
        //Setting defaults
        $error=true;
        $message = 'fail';
        $data=[];
        
        $category_id = $request->input('category_id');
        if($category_id==''){
            $error=true;
            $message = 'Please provide category id';
            $data=[];
        }
        else{

                $packs = DB::table('packages')->where('packcat_id','=',$category_id)->get()->toArray();
                if(count($packs)>0){
                
                $pck = [];
                
                foreach($packs as $p){
                    $p->photo = "https://mockiwiki.com/app/webroot/img/package/".$p->photo;
                    $ipc = [
                        'id'=>$p->id,
                        'name'=>$p->name,
                        'photo'=>$p->photo,
                        'amount'=>$p->amount,
                        'show_amount'=>$p->show_amount,
                        'package_type'=>$p->package_type,
                        'status'=>$p->status
                    ];
                    array_push($pck,$ipc);
                }
                
                $error=false;
                $message = 'success';
                $data=$pck;
                
                }
                else{
                $error=true;
                $message = 'Data not found';
                $data=[];
                }
                
        }
        return response()->json([
            'error' => $error,
            'message' => $message,
            'data'  => $data
        ]);
    }

    public function getPackageDetail(Request $request){
        //Setting defaults
        $error=true;
        $message = 'fail';
        $data=[];
        
        $package_id = $request->input('package_id');
        if($package_id==''){
            $error=true;
            $message = 'Please provide package id';
            $data=[];
        }
        else{
                $packs = DB::table('packages')->where('id','=',$package_id)->get()->toArray();
                if(count($packs)>0){
                $pck = [];
                foreach($packs as $p){
                    $p->photo = "https://mockiwiki.com/app/webroot/img/package/".$p->photo;
                    $ipc = [
                        'id'=>$p->id,
                        'name'=>$p->name,
                        'photo'=>$p->photo,
                        'amount'=>$p->amount,
                        'show_amount'=>$p->show_amount,
                        'package_type'=>$p->package_type,
                        'status'=>$p->status,
                        'description'=>$p->description
                    ];
                    array_push($pck,$ipc);
                }
                
                $error=false;
                $message = 'success';
                $data=$pck;
                
                }
                else{
                $error=true;
                $message = 'Data not found';
                $data=[];
                }
                
        }
        return response()->json([
            'error' => $error,
            'message' => $message,
            'data'  => $data
        ]);
    }

    public function getCheckoutList(Request $request){
        //Setting defaults
        $error=true;
        $message = 'fail';
        $data=[];
        
        $student_id = $request->input('student_id');
        if($student_id==''){
            $error=true;
            $message = 'Please provide student id';
            $data=[];
        }
        else{
                $packs = DB::table('payments')
                            ->join('packages_payments', 'payments.id', '=', 'packages_payments.payment_id')
                            ->join('packages', 'packages_payments.package_id', '=', 'packages.id')
                            ->select('payments.remarks','packages_payments.quantity','packages.name','packages.description','packages.amount','packages.show_amount','packages.package_type','photo')
                            ->where('payments.student_id','=',$student_id)
                            ->where('payments.status','=','Cancelled')
                            ->get()->toArray();
                if(count($packs)>0){
                // $pck = [];
                foreach($packs as $p){
                    $p->photo = "https://mockiwiki.com/app/webroot/img/package/".$p->photo;
                }
                
                $error=false;
                $message = 'success';
                $data=$packs;
                                
                }
                else{
                $error=true;
                $message = 'Data not found';
                $data=[];
                }
                
        }
        return response()->json([
            'error' => $error,
            'message' => $message,
            'data'  => $data
        ]);
    }

    public function getPreviousExamList(Request $request){
        //Setting defaults
        $error=true;
        $message = 'fail';
        $data=[];
        
        $student_id = $request->input('student_id');
        if($student_id==''){
            $error=true;
            $message = 'Please provide student id';
            $data=[];
        }
        else{
                $packs = DB::table('exam_results')
                            ->join('exams', 'exams.id', '=', 'exam_results.exam_id')
                            ->join('students', 'students.id', '=', 'exam_results.student_id')
                            ->select('exams.name','exams.start_date','exams.end_date','exam_results.total_question','exam_results.total_attempt','exam_results.total_answered','exam_results.total_marks','exam_results.obtained_marks','exam_results.percent','exam_results.result')
                            ->where('exam_results.student_id','=',$student_id)
                            ->get()->toArray();
                if(count($packs)>0){
                // $pck = [];
                // foreach($packs as $p){
                //     $p->photo = "https://mockiwiki.com/app/webroot/img/package/".$p->photo;
                // }
                
                $error=false;
                $message = 'success';
                $data=$packs;
                                
                }
                else{
                $error=true;
                $message = 'Data not found';
                $data=[];
                }
                
        }
        return response()->json([
            'error' => $error,
            'message' => $message,
            'data'  => $data
        ]);
    }
    
    public function getUpcomingExamList(Request $request){

        $error=true;
        $message = 'fail';
        $data=[];
        
        $student_id = $request->input('student_id');
        if($student_id==''){
            $error=true;
            $message = 'Please provide student id';
            $data=[];
        }
        else{
                $date = date('Y-m-d h:i:s');
                //Getting all exams paid by student
                $packs = DB::table('payments as p')
                            ->join('packages_payments as pck','p.id','=','pck.payment_id')
                            ->join('packages as pk','pck.package_id','=','pk.id')
                            ->join('exams_packages as ep','pk.id','=','ep.package_id')
                            ->join('exams as e','ep.exam_id','=','e.id')
                            ->select('pk.name as package_name','e.name as exam_name','e.start_date','e.end_date','e.id as exam_id','pk.photo')
                            ->where('p.student_id','=',$student_id)
                            // ->where('e.start_date','>=',$date)
                            ->where('e.end_date','>',$date)
                            ->get()->toArray();

                if(count($packs)>0){
                foreach($packs as $p){
                    $p->photo = "https://mockiwiki.com/app/webroot/img/package/".$p->photo;
                }

                    $error=false;
                    $message = 'success';
                    $data=$packs;
                }                

        }
        return response()->json([
            'error' => $error,
            'message' => $message,
            'data'  => $data
        ]);
    }


    public function getPastExamList(Request $request){

        $error=true;
        $message = 'fail';
        $data=[];
        
        $student_id = $request->input('student_id');
        if($student_id==''){
            $error=true;
            $message = 'Please provide student id';
            $data=[];
        }
        else{
                $date = date('Y-m-d h:i:s');
                //Getting all exams paid by student
                $packs = DB::table('payments as p')
                            ->join('packages_payments as pck','p.id','=','pck.payment_id')
                            ->join('packages as pk','pck.package_id','=','pk.id')
                            ->join('exams_packages as ep','pk.id','=','ep.package_id')
                            ->join('exams as e','ep.exam_id','=','e.id')
                            ->join('exam_results as er','er.exam_id','=','e.id')
                            ->select('er.id as exam_result_id','pk.name as package_name','e.name as exam_name','er.start_time as attempt_date','er.total_marks','er.obtained_marks','er.result','er.percent')
                            ->where('p.student_id','=',$student_id)
                            // ->where('e.start_date','>=',$date)
                            // ->where('e.end_date','>',$date)
                            ->get()->toArray();

                if(count($packs)>0){
                // foreach($packs as $p){
                //     $p->photo = "https://mockiwiki.com/app/webroot/img/package/".$p->photo;
                // }

                    $error=false;
                    $message = 'success';
                    $data=$packs;
                }                

        }
        return response()->json([
            'error' => $error,
            'message' => $message,
            'data'  => $data
        ]);
    }
    
    
    // GET EXAM OF ANY TYPE
    // 1 : paid exam , 2 : Free exam. 3: Upcoming exam . 4: Expired exam
    public function getExamlistByType(Request $request){
        $date = date('Y-m-d h:i:s');
        $error=true;
        $message = 'No data found';
        $data=[];
        
        $student_id = $request->input('student_id');
        if($student_id==''){
            $error=true;
            $message = 'Please provide student id';
            $data=[];
        }
        else{
            
            $etype = $request->input('etype');
            
            if($etype!=''){

            if($etype == '1'){
                //Paid exam

                //Getting all exams paid by student
                $packs = DB::table('payments as p')
                            ->join('packages_payments as pck','p.id','=','pck.payment_id')
                            ->join('packages as pk','pck.package_id','=','pk.id')
                            ->join('exams_packages as ep','pk.id','=','ep.package_id')
                            ->join('exams as e','ep.exam_id','=','e.id')
                            ->select('pk.name as package_name','e.name as exam_name','e.start_date','e.end_date','e.id as exam_id','pk.photo')
                            ->where('p.student_id','=',$student_id)
                            ->where('pk.package_type','=','P')
                            // ->where('e.start_date','>=',$date)
                            ->where('e.start_date','<=',$date)
                            ->get()->toArray();

                if(count($packs)>0){
                foreach($packs as $p){
                    $p->photo = "https://mockiwiki.com/app/webroot/img/package/".$p->photo;
                }

                    $error=false;
                    $message = 'success';
                    $data=$packs;
                }                


            }    
            if($etype == '2'){
                //Free exam

                //Getting all exams paid by student
                $packs = DB::table('payments as p')
                            ->join('packages_payments as pck','p.id','=','pck.payment_id')
                            ->join('packages as pk','pck.package_id','=','pk.id')
                            ->join('exams_packages as ep','pk.id','=','ep.package_id')
                            ->join('exams as e','ep.exam_id','=','e.id')
                            ->select('pk.name as package_name','e.name as exam_name','e.start_date','e.end_date','e.id as exam_id','pk.photo')
                            ->where('p.student_id','=',$student_id)
                            ->where('pk.package_type','=','F')
                            // ->where('e.start_date','>=',$date)
                            ->where('e.start_date','<=',$date)
                            ->get()->toArray();

                if(count($packs)>0){
                foreach($packs as $p){
                    $p->photo = "https://mockiwiki.com/app/webroot/img/package/".$p->photo;
                }

                    $error=false;
                    $message = 'success';
                    $data=$packs;
                }                


            }                
            if($etype == '3'){
                //Upcoming exam

                //Getting all exams paid by student
                $packs = DB::table('payments as p')
                            ->join('packages_payments as pck','p.id','=','pck.payment_id')
                            ->join('packages as pk','pck.package_id','=','pk.id')
                            ->join('exams_packages as ep','pk.id','=','ep.package_id')
                            ->join('exams as e','ep.exam_id','=','e.id')
                            ->select('pk.name as package_name','e.name as exam_name','e.start_date','e.id as exam_id','pk.photo')
                            ->where('p.student_id','=',$student_id)
                            // ->where('e.start_date','>=',$date)
                            ->where('e.end_date','>',$date)
                            ->get()->toArray();

                if(count($packs)>0){
                foreach($packs as $p){
                    $p->photo = "https://mockiwiki.com/app/webroot/img/package/".$p->photo;
                }

                    $error=false;
                    $message = 'success';
                    $data=$packs;
                }                


            }    
            if($etype == '4'){
                //expired exam

                //Getting all exams paid by student
                $packs = DB::table('payments as p')
                            ->join('packages_payments as pck','p.id','=','pck.payment_id')
                            ->join('packages as pk','pck.package_id','=','pk.id')
                            ->join('exams_packages as ep','pk.id','=','ep.package_id')
                            ->join('exams as e','ep.exam_id','=','e.id')
                            ->select('pk.name as package_name','e.name as exam_name','e.end_date','e.id as exam_id','pk.photo')
                            ->where('p.student_id','=',$student_id)
                            // ->where('e.start_date','>=',$date)
                            ->where('e.end_date','<',$date)
                            ->get()->toArray();

                if(count($packs)>0){
                    $expd = [];
                    foreach($packs as $pk){
                        //getting result data
                $exms = DB::table('exam_results as er')
                            ->select('er.id')
                            ->where('er.student_id','=',$student_id)
                            ->where('er.exam_id','=',$pk->exam_id)
                            ->get()->toArray();
                    
                    if(count($exms)==0){
                    array_push($expd,$pk);                        
                    }

                    }
                foreach($expd as $p){
                    $p->photo = "https://mockiwiki.com/app/webroot/img/package/".$p->photo;
                }

                    $error=false;
                    $message = 'success';
                    $data=$expd;
                }                

            }    


                
            }
            if($etype==''){
                $error=true;
                $message = 'Please provide type id';
                $data=[];
            }
            
            

        }
        return response()->json([
            'error' => $error,
            'message' => $message,
            'data'  => $data
        ]);

    }

    public function getStudentResultList(Request $request){

        $error=true;
        $message = 'fail';
        $data=[];
        
        $student_id = $request->input('student_id');
        if($student_id==''){
            $error=true;
            $message = 'Please provide student id';
            $data=[];
        }
        else{
                $date = date('Y-m-d h:i:s');
                //Getting all exams paid by student
                $packs = DB::table('exams as e')
                            ->join('exam_results as er','er.exam_id','=','e.id')
                            ->select('er.id as exam_result_id','e.name as exam_name','er.start_time as attempt_date','er.total_marks','er.obtained_marks','er.result','er.percent')
                            ->where('er.student_id','=',$student_id)
                            ->get()->toArray();

                if(count($packs)>0){
                    $error=false;
                    $message = 'success';
                    $data=$packs;
                }                

        }
        return response()->json([
            'error' => $error,
            'message' => $message,
            'data'  => $data
        ]);
    }

    public function getExamDetail(Request $request){
        //Setting defaults
        $error=true;
        $message = 'fail';
        $data=[];
        
        $exam_id = $request->input('exam_id');
        if($exam_id==''){
            $error=true;
            $message = 'Please provide exam id';
            $data=[];
        }
        else{
                $packs = DB::table('exams')->where('id','=',$exam_id)->get()->toArray();
                if(count($packs)>0){
                $pck = [];

                // getting subjects
                $subs = DB::table('exam_questions as eq')
                           ->join('questions as q','eq.question_id','=','q.id')
                           ->join('subjects as s','s.id','=','q.subject_id')
                           ->where('eq.exam_id','=',$exam_id)
                           ->select('s.subject_name')
                           ->groupBy('s.subject_name')
                           ->get()->toArray();
                
                $subjects = [];
                foreach($subs as $s){
                    array_push($subjects,$s->subject_name);
                }
                
                //Subject questions
                $subques = DB::table('exam_questions as eq')
                           ->join('questions as q','eq.question_id','=','q.id')
                           ->join('subjects as s','s.id','=','q.subject_id')
                           ->where('eq.exam_id','=',$exam_id)
                           ->select('s.subject_name as subject_name','s.id','q.marks')
                           ->orderBy('subject_name','asc')
                           ->get()->toArray();

                $totalquest=count($subques);
                $total_marks=0;
                $subject_count_arr=[];                
                foreach($subjects as $s){
                    $subcount=0;
                    foreach($subques as $sq){
                        if($sq->subject_name == $s){
                            $subcount++;
                        }
                    $total_marks += $sq->marks;                    
                    }
                    $ir = [
                        'subject'=>$s,
                        'count'=>$subcount
                    ];
                    array_push($subject_count_arr,$ir);
                }
                
// select DISTINCT s.subject_name,eq.exam_id from 
// 	exam_questions as eq 
// 	inner join questions as q on eq.question_id=q.id 
//     inner join subjects as s on s.id=q.subject_id
// where eq.exam_id=36
                
                foreach($packs as $p){
                    $ipc = [
                        'name'=>$p->name,
                        'instruction'=>$p->instruction,
                        'syllabus'=>$p->syllabus,
                        'duration'=>$p->duration,
                        "start_date"=> $p->duration,
                        "end_date" => $p->end_date,
                        "passing_percent" => $p->passing_percent,
                        "negative_marking"=> $p->negative_marking,
                        "attempt_count"=> $p->attempt_count,
                        "declare_result"=> $p->declare_result,
                        "finish_result"=> $p->finish_result,
                        "ques_random"=> $p->ques_random,
                        "paid_exam"=> $p->paid_exam,
                        "instant_result"=> $p->instant_result,
                        "option_shuffle"=> $p->option_shuffle,
                        "amount"=> $p->amount,
                        "type"=> $p->type,
                        "multi_language"=> $p->multi_language,
                        'total'=>$totalquest,
                        'subject_questions'=>$subject_count_arr,
                        // 'total_marks'=>$total_marks
                    ];
                    array_push($pck,$ipc);
                }
                
                $error=false;
                $message = 'success';
                $data=$pck;
                
                }
                else{
                $error=true;
                $message = 'Data not found';
                $data=[];
                }
                
        }
        return response()->json([
            'error' => $error,
            'message' => $message,
            'data'  => $data
        ]);
    }
    
    public function getExamList(Request $request){
        //Setting defaults
        $error=true;
        $message = 'fail';
        $data=[];
        
        $exam_id = $request->input('exam_id');
        $student_id = $request->input('student_id');
        if($exam_id=='' && $student_id==''){
            $error=true;
            $message = 'Please provide exam id and student id';
            $data=[];
        }
        else{
                $packs = DB::table('exam_questions')->where('exam_id','=',$exam_id)->get()->toArray();
                if(count($packs)>0){
                    $questions=[];
                    foreach($packs as $p){
                        $quests = DB::table('questions as q')
                                     ->join('qtypes as s','s.id','=','q.qtype_id')
                                     ->join('diffs as d','q.diff_id','=','d.id')
                                     ->select('q.id as question_id','q.subject_id','q.question','q.option1','q.option2','q.option3','q.option4','q.option5','q.option6','q.marks','q.negative_marks','q.hint','q.explanation','q.answer','q.true_false','q.fill_blank','q.subject_id','s.question_type','d.diff_level','d.type')
                                    ->where('q.id','=',$p->question_id)
                                    ->get()->toArray();
                        array_push($questions,$quests);
                    }
                    $data['questions']=$questions;
                $error=true;
                $message = 'success';
                $data=$data;
                }
                else{
                $error=true;
                $message = 'Data not found';
                $data=[];
                }
                
        }
        return response()->json([
            'error' => $error,
            'message' => $message,
            'data'  => $data
        ]);
    }
    
    // public function index()
    // {
    //     $student =  Student::first();
    //     return response()->json([
    //         'error' => false,
    //         'data'  => [
    //             'student' => $student
    //         ]
    //     ]);
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        //
    }
}
