<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all() ,[
            'email' => 'required|email',
            'password' => 'required|string|min:8',
        ]);

        if ($validator->fails()) {
            return \response()->json(['error' => $validator->errors()], 400);
        }

        $token_validity = 24*60;
        $this->guard()->factory()->setTTL($token_validity);

        if (!$token = $this->guard()->attempt($validator->validated())) {
            return \response()->json(['error', 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
        
    }
    public function register(Request $request)
    {
        $validator = Validator::make($request->all() ,[
            'name' => 'required|string|between:3,200',
            'email' => 'required|email|unique:api_users',
            'password' => 'required|string|confirmed|min:8',
        ]);

        if ($validator->fails()) {
            return \response()->json(['error' => $validator->errors()], 422);
        }

        $user = User::create(array_merge($validator->validated(), ['password' => Hash::make($request->password)]));

        return response()->json(['error' => false, 'data' => ['user' => $user]]);
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();
        return response()->json(['error' => false, 'data' => ['message' => 'User logout successfully']]);
    }
    public function profile(Request $request)
    {
        return response()->json($this->guard()->user());
    }
    public function refresh(Request $request)
    {
        return $this->respondWithToken($this->guard()->refresh());
    }

    protected function respondWithToken($token)
    {
        return \response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'token_validity' => $this->guard()->factory()->getTTL() *60 *30

        ]);
    }


   

    protected function guard()
    {
        return Auth::guard();
    }
}
