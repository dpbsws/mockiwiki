<?php

function generate_rand($length = 6): ?string
{
    $no = substr(strtoupper(md5(uniqid(rand()))), 0, $length);
    return $no;
}

?>