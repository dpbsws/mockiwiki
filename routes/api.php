<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\StudentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::group(['middleware' => 'api', 'prefix' => 'auth'], function ()
{
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('register', [AuthController::class, 'register']);
    Route::post('logout', [AuthController::class, 'logout']);
    // Route::get('profile', [AuthController::class, 'profile']);
    Route::post('refresh', [AuthController::class, 'refresh']);
});

Route::group(['middleware' => 'api'], function ()
{
    // Route::post('student', [StudentController::class, 'index']);
    Route::post('student-login', [StudentController::class, 'login']);
    Route::post('student-exam-groups', [StudentController::class, 'exam_groups']);
    Route::post('student-reserve-categories', [StudentController::class, 'reserve_categories']);
    Route::post('student-prefer-lang', [StudentController::class, 'prefer_lang']);
    Route::post('getProfileDetail',[StudentController::class,'getProfileDetail'])->name('getProfileDetail');
    Route::post('inboxAllMessageList',[StudentController::class,'inboxAllMessageList'])->name('inboxAllMessageList');    
    Route::post('sentMessageList',[StudentController::class,'sentMessageList'])->name('sentMessageList');
    Route::post('OneToOneMessageList',[StudentController::class,'OneToOneMessageList'])->name('OneToOneMessageList');
    Route::post('/getBannerList',[StudentController::class,'getBannerList'])->name('getBannerList');
    
    Route::post('/getRegistrationFormData',[StudentController::class,'getRegistrationFormData'])->name('getRegistrationFormData');
    Route::post('/studentProfileUpdate',[StudentController::class,'studentProfileUpdate'])->name('studentProfileUpdate');
    Route::post('/composeMail',[StudentController::class,'composeMail'])->name('composeMail');

    Route::post('/getPackageCategoryList',[StudentController::class,'getPackageCategoryList'])->name('getPackageCategoryList');
    Route::post('/getPackagesByCategory',[StudentController::class,'getPackagesByCategory'])->name('getPackagesByCategory');    
    Route::post('/getPackageDetail',[StudentController::class,'getPackageDetail'])->name('getPackageDetail');        
    Route::post('/getCheckoutList',[StudentController::class,'getCheckoutList'])->name('getCheckoutList');
    
    Route::post('/getPreviousExamList',[StudentController::class,'getPreviousExamList'])->name('getPreviousExamList');
    
    Route::post('/getUpcomingExamList',[StudentController::class,'getUpcomingExamList'])->name('getUpcomingExamList');
    
    Route::post('/getPastExamList',[StudentController::class,'getPastExamList'])->name('getPastExamList');

    Route::post('/getExamlistByType',[StudentController::class,'getExamlistByType'])->name('getExamlistByType');

    Route::post('/getExamDetail',[StudentController::class,'getExamDetail'])->name('getExamDetail');
    
    Route::post('/getStudentResultList',[StudentController::class,'getStudentResultList'])->name('getStudentResultList');    

    Route::post('/getExamList',[StudentController::class,'getExamList'])->name('getExamList');    
    
});




